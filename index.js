let express = require('express');
let app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/user', function (req, res) {

    res.send(JSON.stringify({
        'name':'John',
        'age':'29',
        'role':'engineer'
    }));

})

app.listen(3000);